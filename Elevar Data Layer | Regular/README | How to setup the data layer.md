<details><summary>🔰 What is Google Tag Manager?</summary>
Google Tag Manager (GTM) is a tool that allows you to manage and deploy various tracking codes, scripts, and tags on your website or web application without directly modifying the underlying codebase. It's used to streamline the process of adding and updating tags, such as those for analytics, marketing, and other third-party services. While it's commonly used for websites.
</details>

<details><summary>🔰 What is Shopify?</summary>
Shopify is an e-commerce platform that enables individuals and businesses to create and manage online stores. It provides tools for setting up a digital storefront, managing products, processing payments, and handling various aspects of online selling, making it easier for businesses to sell products and services online.
</details>

<details><summary>🔰 What is data layer push?</summary>
A data layer is a JavaScript object or structure that serves as an intermediary between your website or application and various tracking tools, analytics platforms, or marketing tags. It holds important data about user interactions, behaviors, and other contextual information. By using a data layer, you can centralize and organize the data that needs to be shared with different tracking scripts, making it easier to manage and maintain your tracking implementations. This approach also reduces the need for hardcoding tracking codes directly into your website's source code.
</details>


<details><summary>🔰 What is elevar data layer?</summary>
The Elevar Data Layer is a structured and organized system used for collecting and storing data about user interactions and events on a website. It's typically implemented using JavaScript and is designed to provide a consistent and standardized way of capturing information such as user actions, page views, clicks, and more. This data layer serves as a foundation for various marketing and analytics tools to access and utilize the captured data effectively, making it easier to implement and manage tracking codes and tags on a website.
</details>

<details><summary>🔰 Important of having data layer?</summary>
A data layer is crucial because it provides a structured and consistent way to store and share data between your website or application and various tracking tools, like Google Analytics or marketing platforms. It ensures accurate and organized data collection, enabling better tracking, analysis, and customization of user experiences without tightly coupling code changes to tracking requirements.
</details>


# How to Install the Elevar Data Layer on Shopify eCommerce Website

## **Here are the steps:**

### **Steps 1:** 

Login into Shopify admin panel: 
Sales Channel----> Online Store----> Themes ----> Edit Code ----> Layout ----> theme.liquid

Put the data layer call code in the opening head tag

```
  {% render 'elevar-head' %}
```

And also put the data layer call code in the opening body tag 

```
  {% render 'elevar-body-end' %}
```

**Note:** Only use this one if you have shopify plus membership for checkout events. 

And also put the data layer call code in the opening body tag in layout/checkout.liquid

```
  {% render 'elevar-checkout-end' %}
```
## 

### **Step 2:**

Go the the:
Sales Channel----> Online Store----> Themes ---->Edit Code ---->  Snippets ----> Add a new snippet

1. Name: `elevar-head`
   - Code of [elevar-head](https://gitlab.com/alifmahmud/shopify-elevar-data-layer-for-google-tag-manager-by-alif-mahmud/-/blob/main/Elevar%20Data%20Layer%20%7C%20Regular/elevar-head.js?ref_type=heads) snippet 


2. Name: `elevar-body-end`
   - Code of [elevar-body-end](https://gitlab.com/alifmahmud/shopify-elevar-data-layer-for-google-tag-manager-by-alif-mahmud/-/blob/main/Elevar%20Data%20Layer%20%7C%20Regular/elevar-body-end.js?ref_type=heads) snippet 

3. Name: `elevar-checkout-end`
   - Code of [elevar-checkout-end](https://gitlab.com/alifmahmud/shopify-elevar-data-layer-for-google-tag-manager-by-alif-mahmud/-/blob/main/Elevar%20Data%20Layer%20%7C%20Regular/elevar-checkout-end.js?ref_type=heads) snippet 

Copy all the codes from the added files and paste the codes to the same name. make sure you change the Tag Manager Code on all of them `GTM-P4JR9SVQ`

##

### **Step 3:** 

Put the bellow code on Checkout or Purchase Confirmation Page 

Go to Settings ----> Checkout ----> Order processing ----> Additional Script

- Code: [Additional Scripts](https://gitlab.com/alifmahmud/shopify-elevar-data-layer-for-google-tag-manager-by-alif-mahmud/-/blob/main/Elevar%20Data%20Layer%20%7C%20Regular/additional-scripts.js?ref_type=heads)

##


**Note:** This data layer is from [Elevar](https://www.getelevar.com/) I am not the owner of this data layer. you can use this for education purpose. I am not responsive for any cause or any issues. all rights goes to [Elevar](https://www.getelevar.com/).



### **About Me:**

I am Alif Mahmud, a proficient Data-Driven Marketing and Web Analytics specialist. My expertise lies in Ecommerce and Server Side Tracking, encompassing Google PPC, Display Ads, Google Shopping Ads, YouTube Ads, Facebook Ads, Google Tag Manager, Google Analytics 4, Google Data Studio, and Web Analysis. Working independently as a freelancer, I prioritize dedicated effort and don't believe in shortcuts. My focus is on delivering comprehensive solutions that leverage my advanced skills in the field of Data-Driven Marketing and Web Analytics.


### **Contact:**
- [Whatsapp](https://wa.link/edd12s)
- [Facebook](https://www.facebook.com/alifmahmudashk)
- [Freelancer](https://www.freelancer.com/u/Jakaria004)
